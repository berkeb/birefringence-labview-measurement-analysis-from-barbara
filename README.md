If you measured with the Labview software made by Leonard AND saved the raw data and the matrix in a binary format, please open one of the uploaded python codes.

gui_plot_birefringence_single.py was given by Leonard. It will ask you to click on the files you want to analyse. Recommended for few files.

plot_biref_no_ui_folder.py performs the same analysis on ALL the files placed in the folder of interest. The path has to be given in line 220 AND 221.
In the folder you should not have any other files except for the measured raw data and matrix pairs. (No hidden files either! If anything else is present, you should manually remove them from the list. The command should be added to the code after the line of the path.)

Both of these codes save the processed data in the same folder where the SCRIPT is in text files. The intensity is between 0 and 1 without units. The retardance and the angle are both in radiance.

Biref_plotting_1218.m uses the output of the above mentioned python codes. Calculates the retardance into nm and the angle into degrees. Comments can be found in the script.
