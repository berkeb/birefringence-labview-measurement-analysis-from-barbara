close all
clear all

% go to the folder you want to analyse and add it to the path
list = dir;

% open the list, and find the first and last files that start with "out_A" (in my folders there are
% often hidden files at the beginning of the list)
% add the numbers in line 27!

%% Parameters
% Wavelength used
wavelength = 475; %(blue: 475 nm; green: 535 nm; orange: 615 nm; red: 655 nm)

% Processing parameters:
par.plot = 1;       % Show plots while processing
par.save_image = 0; % Save jpg image
par.save_fig   = 1; % Save matlab figure
par.save_data  = 0; % Save data file

% Intensity Mask for retardance
par.mask = 1;       % if you want to mask out points where the transmission is very high or very low (has to be adapted in line 54!!)
par.limit = 0.1;    % the value used in the intensity mask


%% plotting of all the files in the folder
for k=3:17

% Filename (out_A_name):
%name = 'blue2x';   
tic
k
fullname = list(k).name
name = erase(fullname, "out_A_")

%% Read in file


I = readmatrix(sprintf('out_I_%s',name));

data_ret = readmatrix(sprintf('out_R_%s',name));

%R = (data_ret+(pi/2))*wavelength/(2*pi);
R = data_ret*wavelength/(2*pi);

mean(R,'all')

data_angle = readmatrix(sprintf('out_A_%s',name));
A = (data_angle*360)/(2*pi);

if par.mask == 1
for row = 1:size(R)
    for col = 1:size(R)
        if I(row,col) >= par.limit
           R(row,col) = R(row,col);
           A(row,col) = A(row,col);
        else
           R(row,col) = nan; 
           A(row,col) = nan; 
        end
    end
end
end

%% Plotting
if par.plot == 1
figure(12);
imagesc(I)
axis equal tight xy

colormap gray
colorbar
title('Intensity')
xlabel('')
xticks([])
ylabel('')
yticks([])
set(gca, 'FontSize', 16);
set(gcf, 'Color', 'w')

if par.save_image == 1
figure(12);savefig(sprintf('%s_intensity.fig',name));
end

if par.save_image == 1
saveas(figure(12),sprintf('%s_intensity.jpg',name),'jpg');
end

if par.save_data  == 1
writematrix(I,sprintf('%s_intensity.txt',name),'Delimiter','tab')
end


figure(21);
imagesc(R)
axis equal tight xy
colormap gray
colorbar
title('Retardance (nm)')
xlabel('')
xticks([])
ylabel('')
yticks([])
set(gca, 'FontSize', 16);
set(gcf, 'Color', 'w')

if par.save_fig  == 1
figure(21);savefig(sprintf('%s_retardance.fig',name));
end

if par.save_image == 1
saveas(figure(21),sprintf('%s_retardance.jpg',name),'jpg');
end

if par.save_data  == 1
writematrix(R,sprintf('%s_retardance.txt',name),'Delimiter','tab')
end

figure(3);
imagesc(A)
axis equal tight xy

% colormap(cm)
colorbar
%caxis([-90 90])
caxis([40 50])
title('Angle of the fast axis')
xlabel('')
xticks([])
ylabel('')
yticks([])
set(gca, 'FontSize', 16);
set(gcf, 'Color', 'w')

if par.save_fig  == 1
figure(3);savefig(sprintf('%s_angle.fig',name));
end

if par.save_image == 1
saveas(figure(3),sprintf('%s_angle.jpg',name),'jpg');
end

if par.save_data  == 1
writematrix(A,sprintf('%s_angle.txt',name),'Delimiter','tab')
end

end
toc
end